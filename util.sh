#!/bin/bash

group=qt5-es2

if tput setaf 0 &>/dev/null; then
    ALL_OFF="$(tput sgr0)"
    BOLD="$(tput bold)"
    RED="${BOLD}$(tput setaf 1)"
    GREEN="${BOLD}$(tput setaf 2)"
    YELLOW="${BOLD}$(tput setaf 3)"
    BLUE="${BOLD}$(tput setaf 4)"
else
    ALL_OFF="\e[0m"
    BOLD="\e[1m"
    RED="${BOLD}\e[31m"
    GREEN="${BOLD}\e[32m"
    YELLOW="${BOLD}\e[33m"
    BLUE="${BOLD}\e[34m"
fi

msg() {
    local mesg=$1; shift
    printf "\n${YELLOW}${BOLD}:: ${mesg}${ALL_OFF}\n" "$@" >&2
}

msg2() {
    local mesg=$1; shift
    printf "\n${BOLD}:: ${mesg}${ALL_OFF}\n" "$@" >&2
}

clean() {
    for d in src pkg; do
        [[ -e $d ]] && rm -rf $d
    done
}
