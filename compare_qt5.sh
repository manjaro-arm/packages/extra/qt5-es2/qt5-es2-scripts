#!/bin/bash
source util.sh

echo ""
for p in $(cat qt5-es2.list); do
    echo "* $p: Comparing versions."
    reg_ver=$(sudo pacman -Siy "${p/-es2/}" 2>/dev/null | grep "Version" | head -1 | rev | cut -d: -f1 | rev | sed 's/ //')
    es2_ver=$(sudo pacman -Siy "$p" 2>/dev/null | grep "Version" | head -1 | rev | cut -d: -f1 | rev | sed 's/ //')
    echo "  regular version: ${reg_ver}"
    echo "      es2 version: ${es2_ver}"
    [[ $(vercmp "$reg_ver" "$es2_ver") == 1 ]] && msg "! [$p] needs a rebuild !" || echo ""
done
